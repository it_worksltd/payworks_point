# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This Repository is for intergrating the Payworks Point API with any application.
* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

This implements the PayWorks API.

The example PHP code available there implements only one method - 'payworks_point_one_time' -
this method makes payment via PayWorks processor. You can do one time payments for purchases from it.

Requirements - In order to use the api, you will be required to have a company accounts on PayWorks.
The api requires sending your private key as well as email address in the request envelope. 

This code is released under the terms of the ASL2 (Apache Software
License, version 2).

Hosted at http://www.payworks.bs/paywork/payworks_point_one_time

INSTALLATION

Sample code is present at - https://bitbucket.org/it_worksltd/payworks_point/src/740b6c92a6d062e2cc63d9d7cec438475308502b/Sample_Code?at=master&fileviewer=file-view-default

Demo code available at - https://bitbucket.org/it_worksltd/payworks_point/src/ddee76b64ee60251a487574be8ad0843b671f289/Demo_Code?at=master&fileviewer=file-view-default

Error codes for troubleshooting - https://bitbucket.org/it_worksltd/payworks_point/src/c993b78c2923534c3b2f4b22b1a73887be0f930f/Error%20Codes?at=master&fileviewer=file-view-default

Veiw it here
https://www.payworks.bs/services/payworks-point.php

We are updating the sample-code folder for Python, ASP.net and other major technologies.

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact